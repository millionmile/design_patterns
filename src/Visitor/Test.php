<?php

namespace Visitor;

use Visitor\Animal\Monkey;
use Visitor\Animal\Lion;
use Visitor\Animal\Dolphin;
use Visitor\AnimalOperation\Speak;
use Visitor\AnimalOperation\Jump;

class Test
{

    public function test()
    {
        //动物
        $monkey = new Monkey();
        $lion = new Lion();
        $dolphin = new Dolphin();

        //动物的动作
        $speak = new Speak();
        $jump = new Jump();

        //动物进行某个行为
        $monkey->accept($speak);   // Ooh oo aa aa!
        $monkey->accept($jump);    // Jumped 20 feet high! on to the tree!

        $lion->accept($speak);     // Roaaar!
        $lion->accept($jump);      // Jumped 7 feet! Back on the ground!

        $dolphin->accept($speak);  // Tuut tutt tuutt!
        $dolphin->accept($jump);   // Walked on water a little and disappeared
    }
}