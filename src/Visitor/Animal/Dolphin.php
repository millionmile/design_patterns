<?php


namespace Visitor\Animal;


use Visitor\AnimalOperation\AnimalOperation;

class Dolphin implements Animal
{
    public function speak()
    {
        echo 'Tuut tuttu tuutt!';
    }

    public function accept(AnimalOperation $operation)
    {
        //访问当前动物（类）的指定行为（传参）
        $operation->visitDolphin($this);
        echo PHP_EOL;
    }
}