<?php

namespace Visitor\Animal;

use Visitor\AnimalOperation\AnimalOperation;

// Visitee
interface Animal
{
    public function accept(AnimalOperation $operation);
}