<?php


namespace Visitor\Animal;


use Visitor\AnimalOperation\AnimalOperation;

class Lion implements Animal
{
    public function roar()
    {
        echo 'Roaaar!';
    }

    public function accept(AnimalOperation $operation)
    {
        //访问当前动物（类）的指定行为（传参）
        $operation->visitLion($this);
        echo PHP_EOL;
    }
}