<?php


namespace Visitor\Animal;


use Visitor\AnimalOperation\AnimalOperation;

class Monkey implements Animal
{
    public function shout()
    {
        echo 'Ooh oo aa aa!';
    }

    public function accept(AnimalOperation $operation)
    {
        //访问当前动物（类）的指定行为（传参）
        $operation->visitMonkey($this);
        echo PHP_EOL;
    }
}