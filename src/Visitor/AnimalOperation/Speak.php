<?php

namespace Visitor\AnimalOperation;

use Visitor\AnimalOperation\AnimalOperation;
use Visitor\Animal\Monkey;
use Visitor\Animal\Lion;
use Visitor\Animal\Dolphin;

class Speak implements AnimalOperation
{
    public function visitMonkey(Monkey $monkey)
    {
        $monkey->shout();
    }

    public function visitLion(Lion $lion)
    {
        $lion->roar();
    }

    public function visitDolphin(Dolphin $dolphin)
    {
        $dolphin->speak();
    }
}