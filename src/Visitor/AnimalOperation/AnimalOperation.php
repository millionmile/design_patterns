<?php

namespace Visitor\AnimalOperation;

// Visitor
use Visitor\Animal\Dolphin;
use Visitor\Animal\Lion;
use Visitor\Animal\Monkey;

interface AnimalOperation
{
    public function visitMonkey(Monkey $monkey);

    public function visitLion(Lion $lion);

    public function visitDolphin(Dolphin $dolphin);
}