<?php

namespace Visitor\AnimalOperation;

use Visitor\AnimalOperation\AnimalOperation;
use Visitor\Animal\Monkey;
use Visitor\Animal\Lion;
use Visitor\Animal\Dolphin;

class Jump implements AnimalOperation
{
    public function visitMonkey(Monkey $monkey)
    {
        echo 'Jumped 20 feet high! on to the tree!';
    }

    public function visitLion(Lion $lion)
    {
        echo 'Jumped 7 feet! Back on the ground!';
    }

    public function visitDolphin(Dolphin $dolphin)
    {
        echo 'Walked on water a little and disappeared';
    }
}