<?php

namespace ChainOfResponsibilities;

use ChainOfResponsibilities\Account\Bank;
use ChainOfResponsibilities\Account\Bitcoin;
use ChainOfResponsibilities\Account\Paypal;

class Test
{
    public function test()
    {
        // Let's prepare a chain like below
        //付款顺序：$bank->$paypal->$bitcoin
        // 第一个付款账户：bank
        //如果 bank 支付不了，改为 paypal 支付
        //如果 paypal 支付不了，改为 bit coin 支付

        $bank = new Bank(100);          // Bank with balance 100
        $paypal = new Paypal(200);      // Paypal with balance 200
        $bitcoin = new Bitcoin(300);    // Bitcoin with balance 300

        $bank->setNext($paypal);
        $paypal->setNext($bitcoin);

        //尝试使用bank开始支付
        try {
            $bank->pay(500);
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }

        // Output will be
        // ==============
        // Cannot pay using bank. Proceeding ..
        // Cannot pay using paypal. Proceeding ..:
        // Paid 259 using Bitcoin!
    }
}