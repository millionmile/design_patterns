<?php


namespace ChainOfResponsibilities\Account;


class Bitcoin extends Account
{
    protected $balance;

    //账户的余额
    public function __construct(float $balance)
    {
        $this->balance = $balance;
    }
}