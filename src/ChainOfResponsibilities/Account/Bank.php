<?php


namespace ChainOfResponsibilities\Account;


class Bank extends Account
{
    protected $balance;

    //账户的余额
    public function __construct(float $balance)
    {
        $this->balance = $balance;
    }
}