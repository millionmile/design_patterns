<?php


namespace Facade;

use Facade\Computer\Computer;
use Facade\ComputerFacade\ComputerFacade;

class Test
{
    function test()
    {
        $computer = new ComputerFacade(new Computer());
        $computer->turnOn(); // Ouch! Beep beep! Loading.. Ready to be used!
        $computer->turnOff(); // Bup bup buzzz! Haah! Zzzzz
    }
}