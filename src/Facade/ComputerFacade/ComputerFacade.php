<?php


namespace Facade\ComputerFacade;

//电脑门面
use Facade\Computer\Computer;

class ComputerFacade
{
    protected $computer;

    public function __construct(Computer $computer)
    {
        $this->computer = $computer;
    }

    //开机
    public function turnOn()
    {
        //
        $this->computer->getElectricShock();    //通电
        $this->computer->makeSound();           //声音创建
        $this->computer->showLoadingScreen();   //加载显示屏
        $this->computer->bam();                 //显示欺骗性文字给用户
    }

    //关机
    public function turnOff()
    {
        $this->computer->closeEverything();     //关闭所有内容
        $this->computer->pullCurrent();         //关电
        $this->computer->sooth();               //睡觉
    }
}
