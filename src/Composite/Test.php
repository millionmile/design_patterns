<?php

namespace Composite;

use Composite\Employee\Designer;
use Composite\Employee\Developer;
use Composite\Organization\Organization;

class Test
{
    public function test()
    {
        //准备员工对象
        $john = new Developer('John Doe', 12000);
        $jane = new Designer('Jane Doe', 15000);

        //将员工添加进部门中
        $organization = new Organization();
        $organization->addEmployee($john);
        $organization->addEmployee($jane);

        //获取部门所有员工的总工资
        echo "Net salaries: " . $organization->getNetSalaries(); // Net Salaries: 27000

    }
}