<?php

namespace AbstractFactory\DoorFittingExpert;

interface DoorFittingExpert
{
    public function getDescription();
}