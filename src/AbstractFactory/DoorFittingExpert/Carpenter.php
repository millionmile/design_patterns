<?php

namespace AbstractFactory\DoorFittingExpert;

class Carpenter implements DoorFittingExpert
{
    public function getDescription()
    {
        echo 'I can only fit wooden doors' . PHP_EOL;
    }
}