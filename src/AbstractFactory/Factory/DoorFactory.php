<?php

namespace AbstractFactory\Factory;

use AbstractFactory\DoorFittingExpert\DoorFittingExpert;
use AbstractFactory\Door\Door;

interface DoorFactory
{
    public function makeDoor(): Door;

    public function makeFittingExpert(): DoorFittingExpert;
}