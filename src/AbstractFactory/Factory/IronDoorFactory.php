<?php

namespace AbstractFactory\Factory;

use AbstractFactory\Door\Door;
use AbstractFactory\DoorFittingExpert\DoorFittingExpert;
use AbstractFactory\Door\IronDoor;
use AbstractFactory\DoorFittingExpert\Welder;

class IronDoorFactory implements DoorFactory
{
    public function makeDoor(): Door
    {
        return new IronDoor();
    }

    public function makeFittingExpert(): DoorFittingExpert
    {
        return new Welder();
    }
}