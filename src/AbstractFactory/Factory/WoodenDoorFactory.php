<?php

namespace AbstractFactory\Factory;

use AbstractFactory\Door\Door;
use AbstractFactory\Door\WoodenDoor;
use AbstractFactory\DoorFittingExpert\Carpenter;
use AbstractFactory\DoorFittingExpert\DoorFittingExpert;

class WoodenDoorFactory implements DoorFactory
{
    public function makeDoor(): Door
    {
        return new WoodenDoor();
    }

    public function makeFittingExpert(): DoorFittingExpert
    {
        return new Carpenter();
    }
}