<?php

namespace AbstractFactory;

use AbstractFactory\Factory\IronDoorFactory;
use AbstractFactory\Factory\WoodenDoorFactory;

class Test
{
    public function test()
    {
        //木门工厂
        $woodenFactory = new WoodenDoorFactory();

        $door = $woodenFactory->makeDoor();
        $expert = $woodenFactory->makeFittingExpert();

        $door->getDescription();  // Output: I am a wooden door
        $expert->getDescription(); // Output: I can only fit wooden doors

        //铁门工厂
        $ironFactory = new IronDoorFactory();

        $door = $ironFactory->makeDoor();
        $expert = $ironFactory->makeFittingExpert();

        $door->getDescription();  // Output: I am an iron door
        $expert->getDescription(); // Output: I can only fit iron doors

    }
}