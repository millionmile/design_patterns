<?php

namespace FactoryMethod;

use FactoryMethod\HiringManager\DevelopmentManager;
use FactoryMethod\HiringManager\MarketingManager;

class Test
{
    function test()
    {
        $devManager = new DevelopmentManager();
        $devManager->takeInterview(); // Output: Asking about design patterns

        $marketingManager = new MarketingManager();
        $marketingManager->takeInterview(); // Output: Asking about community building.
    }
}