<?php

namespace FactoryMethod\HiringManager;

use FactoryMethod\Interviewer\Interviewer;

//面试官
abstract class HiringManager
{

    //创建面试者
    abstract protected function makeInterviewer(): Interviewer;

    //面试
    public function takeInterview()
    {
        $interviewer = $this->makeInterviewer();
        $interviewer->askQuestions();
    }
}

