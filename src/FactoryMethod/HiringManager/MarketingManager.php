<?php

namespace FactoryMethod\HiringManager;

use FactoryMethod\Interviewer\CommunityExecutive;
use FactoryMethod\Interviewer\Interviewer;

//市场主管
class MarketingManager extends HiringManager
{
    protected function makeInterviewer(): Interviewer
    {
        return new CommunityExecutive();
    }
}
