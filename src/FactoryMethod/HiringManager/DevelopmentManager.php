<?php

namespace FactoryMethod\HiringManager;

//开发主管
use FactoryMethod\Interviewer\Developer;
use FactoryMethod\Interviewer\Interviewer;

class DevelopmentManager extends HiringManager
{
    protected function makeInterviewer(): Interviewer
    {
        return new Developer();
    }
}
