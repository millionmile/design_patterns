<?php

namespace FactoryMethod\Interviewer;

//面试者
interface Interviewer
{
    //回答问题
    public function askQuestions();
}