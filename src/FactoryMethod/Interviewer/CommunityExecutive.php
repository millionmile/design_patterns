<?php

namespace FactoryMethod\Interviewer;

//行政-面试者
class CommunityExecutive implements Interviewer
{
    //回答问题
    public function askQuestions()
    {
        echo 'Asking about community building' . PHP_EOL;
    }
}