<?php

namespace FactoryMethod\Interviewer;

//开发-面试者
class Developer implements Interviewer
{
    //回答问题
    public function askQuestions()
    {
        echo 'Asking about design patterns!' . PHP_EOL;
    }
}