<?php


namespace Singleton\President;


final class President
{
    private static $instance;

    //构造函数设为私有
    private function __construct()
    {
        // Hide the constructor
    }

    public static function getInstance(): President
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    //克隆函数设为私有
    private function __clone()
    {
        // 禁止克隆
    }

    //反序列化函数设为私有
    private function __wakeup()
    {
        // 禁止反序列化  Disable unserialize
    }
}
