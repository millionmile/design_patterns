<?php

namespace Singleton;

use Singleton\President\President;

class Test
{
    public function test()
    {
        $president1 = President::getInstance();
        $president2 = President::getInstance();

        var_dump($president1 === $president2); // true，是同一个单例对象
    }
}