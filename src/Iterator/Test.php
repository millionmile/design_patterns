<?php

namespace Iterator;


use Iterator\Station\RadioStation;
use Iterator\Station\StationList;

class Test
{
    function test()
    {
        $stationList = new StationList();

        //添加频道
        $stationList->addStation(new RadioStation(89));
        $stationList->addStation(new RadioStation(101));
        $stationList->addStation(new RadioStation(102));
        $stationList->addStation(new RadioStation(103.2));

        foreach ($stationList as $station) {
            //获取迭代的频道
            echo $station->getFrequency() . PHP_EOL;
        }
        $stationList->rewind();
        echo 'current: ' . $stationList->current()->getFrequency() . PHP_EOL;

        //移除频道
        $stationList->removeStation(new RadioStation(89)); // Will remove station 89
    }
}