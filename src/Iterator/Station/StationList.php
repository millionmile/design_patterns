<?php


namespace Iterator\Station;

use Countable;
use Iterator;

class StationList implements Countable, Iterator
{
    /** @var RadioStation[] $stations */
    protected $stations = [];

    /** @var int $counter */
    protected $counter;


    //添加频道
    public function addStation(RadioStation $station)
    {
        $this->stations[] = $station;
    }

    //删除频道
    public function removeStation(RadioStation $toRemove)
    {
        $toRemoveFrequency = $toRemove->getFrequency();
        $this->stations = array_filter($this->stations, function (RadioStation $station) use ($toRemoveFrequency) {
            return $station->getFrequency() !== $toRemoveFrequency;
        });
    }

    //频道数
    public function count(): int
    {
        return count($this->stations);
    }


    //当前频道
    public function current(): RadioStation
    {
        return $this->stations[$this->counter];
    }

    //当前key
    public function key()
    {
        return $this->counter;
    }

    //下一个
    public function next()
    {
        $this->counter++;
    }

    //重置
    public function rewind()
    {
        $this->counter = 0;
    }

    //判断频道是否有效
    public function valid(): bool
    {
        return isset($this->stations[$this->counter]);
    }
}