<?php


namespace Decorator\Coffee;


class WhipCoffee implements Coffee
{
    protected $coffee;

    public function __construct(Coffee $coffee)
    {
        $this->coffee = $coffee;
    }

    public function getCost()
    {
        return $this->coffee->getCost() + 5;   //烤咖啡，加5元
    }

    public function getDescription()
    {
        return $this->coffee->getDescription() . ', whip';
    }
}