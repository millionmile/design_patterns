<?php


namespace Decorator\Coffee;


class VanillaCoffee implements Coffee
{
    protected $coffee;

    public function __construct(Coffee $coffee)
    {
        $this->coffee = $coffee;
    }

    public function getCost()
    {
        return $this->coffee->getCost() + 3;    //加香草，加3元
    }

    public function getDescription()
    {
        return $this->coffee->getDescription() . ', vanilla';
    }
}