<?php


namespace Decorator\Coffee;


class MilkCoffee implements Coffee
{
    protected $coffee;

    public function __construct(Coffee $coffee)
    {
        $this->coffee = $coffee;
    }

    public function getCost()
    {
        return $this->coffee->getCost() + 2;    //咖啡加牛奶，原价+2元
    }

    public function getDescription()
    {
        return $this->coffee->getDescription() . ', milk';
    }
}