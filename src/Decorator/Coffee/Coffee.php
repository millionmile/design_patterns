<?php


namespace Decorator\Coffee;

interface Coffee
{
    public function getCost();

    public function getDescription();
}