<?php


namespace Decorator;


use Decorator\Coffee\MilkCoffee;
use Decorator\Coffee\SimpleCoffee;
use Decorator\Coffee\VanillaCoffee;
use Decorator\Coffee\WhipCoffee;

class Test
{
    function test()
    {
        $someCoffee = new SimpleCoffee();
        echo $someCoffee->getCost() . PHP_EOL; // 10
        echo $someCoffee->getDescription() . PHP_EOL; // Simple Coffee

        //普通咖啡+牛奶
        $someCoffee = new MilkCoffee($someCoffee);
        echo $someCoffee->getCost() . PHP_EOL; // 12
        echo $someCoffee->getDescription() . PHP_EOL; // Simple Coffee, milk

        //普通咖啡+牛奶+烤
        $someCoffee = new WhipCoffee($someCoffee);
        echo $someCoffee->getCost() . PHP_EOL; // 17
        echo $someCoffee->getDescription() . PHP_EOL; // Simple Coffee, milk, whip

        //普通咖啡+牛奶+烤+香草
        $someCoffee = new VanillaCoffee($someCoffee);
        echo $someCoffee->getCost() . PHP_EOL; // 20
        echo $someCoffee->getDescription() . PHP_EOL; // Simple Coffee, milk, whip, vanilla

    }
}