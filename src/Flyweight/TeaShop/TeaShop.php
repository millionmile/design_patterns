<?php


namespace Flyweight\TeaShop;


use Flyweight\TeaMaker\TeaMaker;

class TeaShop
{
    protected $orders;
    protected $teaMaker;

    public function __construct(TeaMaker $teaMaker)
    {
        $this->teaMaker = $teaMaker;
    }

    //创建订单，每桌一个订单，最多生成n桌的订单，不会超过太多对象
    public function takeOrder(string $teaType, int $table)
    {
        $this->orders[$table] = $this->teaMaker->make($teaType);
    }

    public function serve()
    {
        //输出订单
        foreach ($this->orders as $table => $tea) {
            echo "Serving tea to table# " . $table . PHP_EOL;
        }
    }
}
