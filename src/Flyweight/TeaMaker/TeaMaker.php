<?php


namespace Flyweight\TeaMaker;

use Flyweight\Tea\KarakTea;

class TeaMaker
{
    protected $availableTea = [];

    public function make($preference)
    {
        //如果做过哪杯茶，那么重复输出，不创建新的茶
        if (empty($this->availableTea[$preference])) {
            $this->availableTea[$preference] = new KarakTea($preference);
        }

        return $this->availableTea[$preference];
    }
}