<?php

namespace Flyweight;


use Flyweight\TeaMaker\TeaMaker;
use Flyweight\TeaShop\TeaShop;

class Test
{
    function test()
    {
        $teaMaker = new TeaMaker();
        $shop = new TeaShop($teaMaker);

        //台号在之后是可以重复的
        $shop->takeOrder('less sugar', 1);
        $shop->takeOrder('more milk', 2);
        $shop->takeOrder('without sugar', 5);

        $shop->serve();
        // Serving tea to table# 1
        // Serving tea to table# 2
        // Serving tea to table# 5

    }
}