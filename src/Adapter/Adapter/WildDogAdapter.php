<?php

namespace Adapter\Adapter;

//野狗仿狮子的适配器
// Adapter around wild dog to make it compatible with our game
use Adapter\Lion\Lion;
use Adapter\WildDog\WildDog;

class WildDogAdapter implements Lion
{
    protected $dog;

    public function __construct(WildDog $dog)
    {
        $this->dog = $dog;
    }

    public function roar()
    {
        $this->dog->bark();
    }
}