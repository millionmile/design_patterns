<?php

namespace Adapter;

use Adapter\Adapter\WildDogAdapter;
use Adapter\Hunter\Hunter;
use Adapter\Lion\AfricanLion;
use Adapter\WildDog\WildDog;
use Builder\BurgerBuilder\BurgerBuilder;

class Test
{
    public function test()
    {
        //非洲狮
        $africanLion = new AfricanLion();
        $hunter = new Hunter();
        //猎人抓狮子，狮子咆哮了一声
        $hunter->hunt($africanLion);

        //野狗
        $wildDog = new WildDog();
        $wildDogAdapter = new WildDogAdapter($wildDog);

        //猎人抓野狗，野狗仿狮子咆哮（吼）了一声
        $hunter->hunt($wildDogAdapter);

    }
}