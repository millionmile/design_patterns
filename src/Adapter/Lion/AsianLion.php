<?php


namespace Adapter\Lion;


class AsianLion implements Lion
{
    public function roar()
    {
        echo 'AsianLion roar' . PHP_EOL;
    }
}