<?php


namespace Adapter\Lion;


class AfricanLion implements Lion
{
    public function roar()
    {
        echo 'AfricanLion roar' . PHP_EOL;
    }
}