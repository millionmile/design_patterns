<?php


namespace Adapter\Hunter;


use Adapter\Lion\Lion;

class Hunter
{
    //抓捕狮子
    public function hunt(Lion $lion)
    {
        //狮子被抓到，咆哮了一声
        $lion->roar();
    }
}