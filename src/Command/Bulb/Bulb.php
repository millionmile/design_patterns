<?php


namespace Command\Bulb;


// Receiver
class Bulb
{
    public function turnOn()
    {
        echo "Bulb has been lit" . PHP_EOL;
    }

    public function turnOff()
    {
        echo "Darkness!" . PHP_EOL;
    }
}
