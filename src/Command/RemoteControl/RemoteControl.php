<?php


namespace Command\RemoteControl;

// Invoker
use Command\Command\Command;

class RemoteControl
{
    public function submit(Command $command)
    {
        $command->execute();
    }
}