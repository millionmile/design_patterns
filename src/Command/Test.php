<?php

namespace Command;

use ChainOfResponsibilities\Account\Bank;
use ChainOfResponsibilities\Account\Bitcoin;
use ChainOfResponsibilities\Account\Paypal;
use Command\Bulb\Bulb;
use Command\Command\TurnOff;
use Command\Command\TurnOn;
use Command\RemoteControl\RemoteControl;

class Test
{
    public function test()
    {
        $bulb = new Bulb();

        $turnOn = new TurnOn($bulb);
        $turnOff = new TurnOff($bulb);

        $remote = new RemoteControl();
        $remote->submit($turnOn); // Bulb has been lit!
        $remote->submit($turnOff); // Darkness!
    }
}