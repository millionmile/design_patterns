<?php


namespace Interpreter\Expression;


class ExpressionCharater implements Expression
{
    function interpreter(string $str): string
    {
        return strtoupper($str);
    }
}