<?php


namespace Interpreter\Expression;


interface Expression
{
    //翻译
    public function interpreter(string $str): string;
}