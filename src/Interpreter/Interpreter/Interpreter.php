<?php


namespace Interpreter\Interpreter;


use Interpreter\Expression\ExpressionCharater;
use Interpreter\Expression\ExpressionNum;

class Interpreter
{
    function execute($string)
    {
        $expression = null;
        for ($i = 0; $i < strlen($string); $i++) {
            $temp = $string[$i];
            //判断要使用解释器的内容
            switch (true) {
                case is_numeric($temp):
                    $expression = new ExpressionNum();
                    break;
                default:
                    $expression = new ExpressionCharater();
            }
            echo $expression->interpreter($temp);
        }
    }
}