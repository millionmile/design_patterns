<?php

namespace Proxy;

use Proxy\Door\LabDoor;
use Proxy\Door\SecuredDoor;

class Test
{
    public function test()
    {
        $door = new SecuredDoor(new LabDoor());
        $door->open('invalid'); // Big no! It ain't possible.

        $door->open('$ecr@t'); // Opening lab door
        $door->close(); // Closing lab door

    }
}