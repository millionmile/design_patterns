<?php


namespace Proxy\Door;


class SecuredDoor
{
    protected $door;

    public function __construct(Door $door)
    {
        $this->door = $door;
    }

    public function open($password)
    {
        //验证密码成功才开门
        if ($this->authenticate($password)) {
            $this->door->open();
        } else {
            echo "Big no! It ain't possible." . PHP_EOL;
        }
    }

    //验证密码
    public function authenticate($password)
    {
        return $password === '$ecr@t';
    }

    //关门
    public function close()
    {
        $this->door->close();
    }
}