<?php


namespace Proxy\Door;


interface Door
{
    public function open();

    public function close();
}