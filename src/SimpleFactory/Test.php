<?php

namespace SimpleFactory;

use SimpleFactory\Factory\DoorFactory;

class Test
{
    public function test()
    {
        // 做一个宽100 高200的门
        $door = DoorFactory::makeDoor(100, 200);

        echo 'door1: ' . PHP_EOL;
        echo 'Width: ' . $door->getWidth() . PHP_EOL;
        echo 'Height: ' . $door->getHeight() . PHP_EOL;

        // 做一个宽50 高100的门
        $door2 = DoorFactory::makeDoor(50, 100);
        echo 'door2: ' . PHP_EOL;
        echo 'Width: ' . $door->getWidth() . PHP_EOL;
        echo 'Height: ' . $door->getHeight() . PHP_EOL;
    }
}