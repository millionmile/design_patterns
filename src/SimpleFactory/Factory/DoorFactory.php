<?php

namespace SimpleFactory\Factory;

use SimpleFactory\Door\Door;
use SimpleFactory\Door\WoodenBurger;

class DoorFactory
{
    public static function makeDoor($width, $height): Door
    {
        return new WoodenBurger($width, $height);
    }
}