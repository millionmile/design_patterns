<?php

namespace SimpleFactory\Door;

interface Door
{
    public function getWidth(): float;

    public function getHeight(): float;
}