<?php


namespace Observer\Job;


interface Observer
{
    public function onJobPosted(JobPost $job);
}