<?php


namespace State\TextEditor;


use State\WritingState\WritingState;

class TextEditor
{
    protected $state;

    public function __construct(WritingState $state)
    {
        $this->state = $state;
    }

    public function setState(WritingState $state)
    {
        $this->state = $state;
    }

    public function type(string $words)
    {
        $this->state->write($words);
        echo PHP_EOL;
    }
}
