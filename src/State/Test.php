<?php


namespace State;


use State\TextEditor\TextEditor;
use State\WritingState\DefaultText;
use State\WritingState\LowerCase;
use State\WritingState\UpperCase;

class Test
{
    function test()
    {
        $editor = new TextEditor(new DefaultText());

        $editor->type('First line');

        $editor->setState(new UpperCase());

        $editor->type('Second line');
        $editor->type('Third line');

        $editor->setState(new LowerCase());

        $editor->type('Fourth line');
        $editor->type('Fifth line');

// Output:
// First line
// SECOND LINE
// THIRD LINE
// fourth line
// fifth line

    }
}