<?php


namespace State\WritingState;

interface WritingState
{
    public function write(string $words);
}