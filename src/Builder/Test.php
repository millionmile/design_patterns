<?php

namespace Builder;

use Builder\BurgerBuilder\BurgerBuilder;

class Test
{
    public function test()
    {
        $burger = (new BurgerBuilder(14))
            ->addPepperoni()
            ->addLettuce()
            ->addTomato()
            ->build();
        print_r($burger);
    }
}