<?php

namespace Mediator;

use Mediator\ChatRoom\ChatRoom;
use Mediator\User\User;

class Test
{
    function test()
    {
        $mediator = new ChatRoom();

        $john = new User('John Doe', $mediator);
        $jane = new User('Jane Doe', $mediator);

        $john->send('Hi there!' . PHP_EOL);
        $jane->send('Hey!' . PHP_EOL);

        // Output will be
        // Feb 14, 10:58 [John]: Hi there!
        // Feb 14, 10:58 [Jane]: Hey!
    }
}