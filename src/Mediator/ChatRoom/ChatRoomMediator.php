<?php


namespace Mediator\ChatRoom;


use Mediator\User\User;

interface ChatRoomMediator
{
    public function showMessage(User $user, string $message);
}