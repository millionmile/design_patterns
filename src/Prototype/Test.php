<?php

namespace Prototype;

use Prototype\Sheep\Sheep;

class Test
{
    public function test()
    {
        $original = new Sheep('Jolly');
        echo $original->getName() . PHP_EOL; // Jolly
        echo $original->getCategory() . PHP_EOL; // Mountain Sheep

        // Clone and modify what is required
        //也可以使用魔术方法__clone来修改克隆行为
        $cloned = clone $original;
        $cloned->setName('Dolly');
        $cloned->setCategory('Clone sheep');
        echo $cloned->getName() . PHP_EOL; // Dolly
        echo $cloned->getCategory() . PHP_EOL; // Mountain sheep

    }
}