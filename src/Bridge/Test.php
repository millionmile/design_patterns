<?php

namespace Bridge;

use Bridge\Theme\DarkTheme;
use Bridge\WebPage\About;
use Bridge\WebPage\Careers;

class Test
{
    public function test()
    {
        $darkTheme = new DarkTheme();

        $about = new About($darkTheme);
        $careers = new Careers($darkTheme);

        echo $about->getContent(); // "About page in Dark Black";
        echo $careers->getContent(); // "Careers page in Dark Black";
    }
}