<?php


namespace Bridge\WebPage;


use Bridge\Theme\Theme;

interface WebPage
{
    public function __construct(Theme $theme);

    public function getContent();
}