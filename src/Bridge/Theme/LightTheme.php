<?php


namespace Bridge\Theme;


class LightTheme implements Theme
{
    public function getColor()
    {
        return 'Off white';
    }
}