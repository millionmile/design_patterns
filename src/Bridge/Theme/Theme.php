<?php


namespace Bridge\Theme;


interface Theme
{
    public function getColor();
}