<?php


namespace Bridge\Theme;


class DarkTheme implements Theme
{
    public function getColor()
    {
        return 'Dark Black';
    }
}