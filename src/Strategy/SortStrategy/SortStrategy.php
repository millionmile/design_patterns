<?php


namespace Strategy\SortStrategy;


interface SortStrategy
{
    public function sort(array $dataset): array;
}