<?php


namespace Strategy;


use Strategy\Sorter\Sorter;
use Strategy\SortStrategy\BubbleSortStrategy;
use Strategy\SortStrategy\QuickSortStrategy;

class Test
{
    function test()
    {
        $dataset = [1, 5, 4, 3, 2, 8];

        $sorter = new Sorter(new BubbleSortStrategy());
        $sorter->sort($dataset); // Output : Sorting using bubble sort
        echo PHP_EOL;

        $sorter = new Sorter(new QuickSortStrategy());
        $sorter->sort($dataset); // Output : Sorting using quick sort
        echo PHP_EOL;

    }
}