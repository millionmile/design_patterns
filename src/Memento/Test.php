<?php


namespace Memento;


use Memento\Editor\Editor;

class Test
{
    function test()
    {
        $editor = new Editor();

// Type some stuff
        $editor->type('This is the first sentence.');
        $editor->type('This is second.');

        // Save the state to restore to : This is the first sentence. This is second.
        $saved = $editor->save();
        // Type some more
        $editor->type('And this is third.');

        // Output: Content before Saving
        echo $editor->getContent(); // This is the first sentence. This is second. And this is third.
        echo PHP_EOL;

        // Restoring to last saved state
        $editor->restore($saved);

        echo $editor->getContent(); // This is the first sentence. This is second.
        echo PHP_EOL;
    }
}